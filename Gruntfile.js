/*global module:false*/
module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        // Metadata.
        pkg: grunt.file.readJSON('package.json'),
        // Task configuration.
        concat: {
            basic: {
                src: ['css/*.css'],
                dest: 'dist/css/<%= pkg.name + pkg.version %>.css'
            }
        },
        cssmin: {
            combine: {
                files: {
                    'dist/css/<%= pkg.name + pkg.version %>.min.css': ['css/*.css']
                }
            }
        },
        watch: {
            cssfiles: {
                files: 'css/*.css',
                tasks: ['concat:basic', 'cssmin']
            },
            html: {
                files: 'index.html'
            },
            options: {
                livereload: {
                    host: 'localhost',
                    port: 35729,
                }
            }
        }
    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    // Default task.
    grunt.registerTask('default', ['concat', 'cssmin']);
};
